package org.openstreetmap.josm.plugins.damn.actions;

import static org.openstreetmap.josm.gui.help.HelpUtil.ht;
import static org.openstreetmap.josm.tools.I18n.tr;

import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.net.URL;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;

import org.openstreetmap.josm.actions.JosmAction;
import org.openstreetmap.josm.spi.preferences.Config;
import org.openstreetmap.josm.tools.HttpClient;

import org.openstreetmap.josm.plugins.damn.callbacks.GetAreasCallback;

/**
 * Get areas from damn server.
 *
 * @author qeef
 * @since xxx
 */
public class GetAreasAction extends JosmAction {
    private String damn_url = Config.getPref().get("damn.server_url");
    private String damn_token = Config.getPref().get("damn.token");
    private GetAreasCallback callback;

    public GetAreasAction(GetAreasCallback c)
    {
        super(
            tr("Get areas"),
            "actions/areas",
            tr("Get areas from damn server."),
            null,
            true
        );
        callback = c;
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        try {
            HttpClient g = HttpClient.create(new URL(damn_url + "/areas"));
            g.connect();
            BufferedReader br = g.getResponse().getContentReader();
            JsonReader r = Json.createReader(br);
            JsonArray l = r.readArray();
            r.close();
            callback.onGetAreas(l);
        } catch (Exception ex) {
        }
    }
}
