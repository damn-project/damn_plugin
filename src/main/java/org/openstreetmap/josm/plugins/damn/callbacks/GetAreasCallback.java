package org.openstreetmap.josm.plugins.damn.callbacks;

import javax.json.JsonArray;

public interface GetAreasCallback {
    void onGetAreas(JsonArray l);
}
