package org.openstreetmap.josm.plugins.damn.callbacks;

import javax.json.JsonObject;

public interface PostDoneSquareCallback {
    void onPostDoneSquare(JsonObject doneSquare, JsonObject gotCommit);
}
