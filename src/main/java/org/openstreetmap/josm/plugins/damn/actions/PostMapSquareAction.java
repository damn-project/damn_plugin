package org.openstreetmap.josm.plugins.damn.actions;

import static org.openstreetmap.josm.gui.help.HelpUtil.ht;
import static org.openstreetmap.josm.tools.I18n.tr;

import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.net.URL;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.swing.JOptionPane;

import org.openstreetmap.josm.actions.JosmAction;
import org.openstreetmap.josm.spi.preferences.Config;
import org.openstreetmap.josm.tools.HttpClient;

import org.openstreetmap.josm.plugins.damn.callbacks.PostMapSquareCallback;
import org.openstreetmap.josm.plugins.damn.DamnDialog;

/**
 * Map square again query to damn server.
 *
 * @author qeef
 * @since xxx
 */
public class PostMapSquareAction extends JosmAction {
    private String damn_url = Config.getPref().get("damn.server_url");
    private String damn_token = Config.getPref().get("damn.token");
    private PostMapSquareCallback callback;
    JsonObject mapSquare;

    public PostMapSquareAction(PostMapSquareCallback c, JsonObject jo)
    {
        super(
            tr("Map again"),
            "actions/map",
            tr("Map square again query to damn server."),
            null,
            true
        );
        callback = c;
        mapSquare = jo;
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (!DamnDialog.removeDamnLayers())
            return;
        try {
            HttpClient g = HttpClient.create(
                new URL(
                    damn_url
                    + "/area/" + mapSquare.getInt("aid")
                    + "/commits"
                ),
                "POST"
            );
            g.setHeader("Content-Type", "application/json");
            g.setHeader("Authorization", "Bearer " + damn_token);
            String msg;
            msg = "Map this square again as it is not mapped well yet.";
            msg = JOptionPane.showInputDialog("Post message", msg);
            JsonObject map_square = Json.createObjectBuilder()
                .add("sid", mapSquare.getInt("sid"))
                .add("type", "needs mapping")
                .add("message", msg)
            .build();
            g.setRequestBody(map_square.toString().getBytes());
            g.connect();
            BufferedReader br = g.getResponse().getContentReader();
            JsonReader r = Json.createReader(br);
            JsonObject gotCommit = r.readObject();
            r.close();
            callback.onPostMapSquare(mapSquare, gotCommit);
        } catch (Exception ex) {
        }
    }
}
