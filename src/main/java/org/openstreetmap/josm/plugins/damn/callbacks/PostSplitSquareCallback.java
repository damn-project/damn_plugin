package org.openstreetmap.josm.plugins.damn.callbacks;

import javax.json.JsonObject;

public interface PostSplitSquareCallback {
    void onPostSplitSquare(JsonObject splitSquare, JsonObject splittedSquare);
}
