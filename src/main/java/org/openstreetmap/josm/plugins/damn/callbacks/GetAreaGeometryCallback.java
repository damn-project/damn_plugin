package org.openstreetmap.josm.plugins.damn.callbacks;

import javax.json.JsonObject;

public interface GetAreaGeometryCallback {
    void onGetAreaGeometry(JsonObject gotAreaGeometry);
}
