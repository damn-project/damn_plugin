package org.openstreetmap.josm.plugins.damn.actions;

import static org.openstreetmap.josm.gui.help.HelpUtil.ht;
import static org.openstreetmap.josm.tools.I18n.tr;

import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.net.URL;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;

import org.openstreetmap.josm.actions.JosmAction;
import org.openstreetmap.josm.spi.preferences.Config;
import org.openstreetmap.josm.tools.HttpClient;

import org.openstreetmap.josm.plugins.damn.callbacks.PostMapAreaCallback;

/**
 * Post map area query to damn server.
 *
 * @author qeef
 * @since xxx
 */
public class PostMapAreaAction extends JosmAction {
    private String damn_url = Config.getPref().get("damn.server_url");
    private String damn_token = Config.getPref().get("damn.token");
    private PostMapAreaCallback callback;
    JsonObject mapArea;

    public PostMapAreaAction(PostMapAreaCallback c, JsonObject jo)
    {
        super(
            tr("Map"),
            "actions/map",
            tr("Post map area query to damn server."),
            null,
            true
        );
        callback = c;
        mapArea = jo;
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        try {
            HttpClient g = HttpClient.create(
                new URL(
                    damn_url
                    + "/area/"
                    + mapArea.getInt("aid")
                    + "/commits"
                ),
                "POST"
            );
            g.setHeader("Content-Type", "application/json");
            g.setHeader("Authorization", "Bearer " + damn_token);
            String ma = Config.getPref().get("damn.map_action");
            JsonObject map_area = Json.createObjectBuilder()
                .add("type", ma)
            .build();
            g.setRequestBody(map_area.toString().getBytes());
            g.connect();
            BufferedReader br = g.getResponse().getContentReader();
            JsonReader r = Json.createReader(br);
            JsonObject gotSquare = r.readObject();
            r.close();
            callback.onPostMapArea(mapArea, gotSquare);
        } catch (Exception ex) {
        }
    }
}
