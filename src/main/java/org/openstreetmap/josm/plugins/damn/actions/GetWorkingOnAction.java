package org.openstreetmap.josm.plugins.damn.actions;

import static org.openstreetmap.josm.gui.help.HelpUtil.ht;
import static org.openstreetmap.josm.tools.I18n.tr;

import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;

import org.openstreetmap.josm.actions.JosmAction;
import org.openstreetmap.josm.spi.preferences.Config;
import org.openstreetmap.josm.tools.HttpClient;

import org.openstreetmap.josm.plugins.damn.callbacks.GetWorkingOnCallback;
import org.openstreetmap.josm.plugins.damn.DamnDialog;

/**
 * Get working on from damn server.
 *
 * @author qeef
 * @since xxx
 */
public class GetWorkingOnAction extends JosmAction {
    private String damn_url = Config.getPref().get("damn.server_url");
    private String damn_token = Config.getPref().get("damn.token");
    private GetWorkingOnCallback callback;

    public GetWorkingOnAction(GetWorkingOnCallback c)
    {
        super(
            tr("Working on"),
            "actions/workingon",
            tr("Get what I am working on from damn server."),
            null,
            true
        );
        callback = c;
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        try {
            Date now = new Date(
                System.currentTimeMillis()
                - 1000 * 60 * 60 * 24 * 7 // week ago
            );
            String now_fs = "yyyy-MM-dd'T'HH:mm:ss.S";
            SimpleDateFormat now_f = new SimpleDateFormat(now_fs);
            now_f.setTimeZone(TimeZone.getTimeZone("UTC"));
            JsonObject jwt = DamnDialog.get_jwt(damn_token);
            String dn = jwt.getString("display_name");
            HttpClient g = HttpClient.create(new URL(
                damn_url + "/user/" + dn + "/commits"
                + "?that_are_distinct=1"));
            g.connect();
            BufferedReader br = g.getResponse().getContentReader();
            JsonReader r = Json.createReader(br);
            JsonArray l = r.readArray();
            r.close();
            callback.onGetWorkingOn(l);
        } catch (Exception ex) {
        }
    }
}
