package org.openstreetmap.josm.plugins.damn.callbacks;

import javax.json.JsonObject;

public interface PostMapSquareCallback {
    void onPostMapSquare(JsonObject mapSquare, JsonObject gotCommit);
}
