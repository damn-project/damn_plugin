package org.openstreetmap.josm.plugins.damn.callbacks;

import javax.json.JsonObject;

public interface PostMapAreaCallback {
    void onPostMapArea(JsonObject mapArea, JsonObject gotSquare);
}
