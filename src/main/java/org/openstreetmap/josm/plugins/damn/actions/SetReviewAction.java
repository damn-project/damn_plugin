package org.openstreetmap.josm.plugins.damn.actions;

import static org.openstreetmap.josm.gui.help.HelpUtil.ht;
import static org.openstreetmap.josm.tools.I18n.tr;

import java.awt.event.ActionEvent;

import org.openstreetmap.josm.actions.JosmAction;
import org.openstreetmap.josm.spi.preferences.Config;

/**
 * Set default _review_ action (new lock action).
 *
 * @author qeef
 * @since xxx
 */
public class SetReviewAction extends JosmAction {
    private String what;

    /**
     * Constructs a new {@code SetReviewAction}.
     */
    public SetReviewAction(String w)
    {
        super(
            tr("Set review action to " + w),
            "actions/review",
            tr("Set review action to " + w),
            null,
            true
        );
        what = w;
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        Config.getPref().put("damn.review_action", what);
    }
}
