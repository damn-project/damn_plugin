package org.openstreetmap.josm.plugins.damn.actions;

import static org.openstreetmap.josm.gui.help.HelpUtil.ht;
import static org.openstreetmap.josm.tools.I18n.tr;

import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.net.URL;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;

import org.openstreetmap.josm.actions.JosmAction;
import org.openstreetmap.josm.spi.preferences.Config;
import org.openstreetmap.josm.tools.HttpClient;

import org.openstreetmap.josm.plugins.damn.callbacks.GetAreaCallback;

/**
 * Get area from damn server.
 *
 * @author qeef
 * @since xxx
 */
public class GetAreaAction extends JosmAction {
    private String damn_url = Config.getPref().get("damn.server_url");
    private String damn_token = Config.getPref().get("damn.token");
    private GetAreaCallback callback;
    private JsonObject callArea;

    public GetAreaAction(GetAreaCallback c, JsonObject jo)
    {
        super(
            tr("Get area"),
            "actions/area",
            tr("Get area from damn server."),
            null,
            true
        );
        callback = c;
        callArea = jo;
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        try {
            HttpClient g = HttpClient.create(new URL(
                damn_url
                + "/area/"
                + callArea.getInt("aid")
            ));
            g.connect();
            BufferedReader br = g.getResponse().getContentReader();
            JsonReader r = Json.createReader(br);
            JsonObject gotArea = r.readObject();
            r.close();
            callback.onGetArea(callArea, gotArea);
        } catch (Exception ex) {
        }
    }
}
