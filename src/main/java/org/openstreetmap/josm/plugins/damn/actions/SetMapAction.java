package org.openstreetmap.josm.plugins.damn.actions;

import static org.openstreetmap.josm.gui.help.HelpUtil.ht;
import static org.openstreetmap.josm.tools.I18n.tr;

import java.awt.event.ActionEvent;

import org.openstreetmap.josm.actions.JosmAction;
import org.openstreetmap.josm.spi.preferences.Config;

/**
 * Set default _map_ action (new lock action).
 *
 * @author qeef
 * @since xxx
 */
public class SetMapAction extends JosmAction {
    private String what;

    /**
     * Constructs a new {@code SetMapAction}.
     */
    public SetMapAction(String w)
    {
        super(
            tr("Set map action to " + w),
            "actions/map",
            tr("Set map action to " + w),
            null,
            true
        );
        what = w;
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        Config.getPref().put("damn.map_action", what);
    }
}
