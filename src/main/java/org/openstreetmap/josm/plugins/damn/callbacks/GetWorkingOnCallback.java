package org.openstreetmap.josm.plugins.damn.callbacks;

import javax.json.JsonArray;

public interface GetWorkingOnCallback {
    void onGetWorkingOn(JsonArray l);
}
