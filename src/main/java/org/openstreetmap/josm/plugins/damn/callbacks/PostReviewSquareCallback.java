package org.openstreetmap.josm.plugins.damn.callbacks;

import javax.json.JsonObject;

public interface PostReviewSquareCallback {
    void onPostReviewSquare(JsonObject mapSquare, JsonObject gotCommit);
}
