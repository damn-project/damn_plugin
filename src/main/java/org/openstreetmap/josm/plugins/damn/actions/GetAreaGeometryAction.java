package org.openstreetmap.josm.plugins.damn.actions;

import static org.openstreetmap.josm.gui.help.HelpUtil.ht;
import static org.openstreetmap.josm.tools.I18n.tr;

import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.net.URL;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;

import org.openstreetmap.josm.actions.JosmAction;
import org.openstreetmap.josm.spi.preferences.Config;
import org.openstreetmap.josm.tools.HttpClient;

import org.openstreetmap.josm.plugins.damn.callbacks.GetAreaGeometryCallback;

/**
 * Get area from damn server.
 *
 * @author qeef
 * @since xxx
 */
public class GetAreaGeometryAction extends JosmAction {
    private String damn_url = Config.getPref().get("damn.server_url");
    private String damn_token = Config.getPref().get("damn.token");
    private GetAreaGeometryCallback callback;
    private JsonObject callArea;

    public GetAreaGeometryAction(GetAreaGeometryCallback c, JsonObject jo)
    {
        super(
            tr("Get area geometry"),
            "actions/border",
            tr("Get area geometry from the damn server."),
            null,
            true
        );
        callback = c;
        callArea = jo;
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        try {
            HttpClient g = HttpClient.create(new URL(
                damn_url
                + "/area/"
                + String.valueOf(callArea.getInt("aid"))
                + "/geometry"
            ));
            g.connect();
            BufferedReader br = g.getResponse().getContentReader();
            JsonReader r = Json.createReader(br);
            JsonObject gotAreaGeometry = r.readObject();
            r.close();
            callback.onGetAreaGeometry(gotAreaGeometry);
        } catch (Exception ex) {
        }
    }
}
