package org.openstreetmap.josm.plugins.damn.callbacks;

import javax.json.JsonObject;

public interface PostReviewAreaCallback {
    void onPostReviewArea(JsonObject reviewArea, JsonObject gotSquare);
}
