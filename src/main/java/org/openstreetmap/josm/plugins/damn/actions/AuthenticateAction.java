package org.openstreetmap.josm.plugins.damn.actions;

import static org.openstreetmap.josm.gui.help.HelpUtil.ht;
import static org.openstreetmap.josm.tools.I18n.tr;

import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.net.URL;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.Timer;
import java.util.TimerTask;

import org.openstreetmap.josm.actions.JosmAction;
import org.openstreetmap.josm.spi.preferences.Config;
import org.openstreetmap.josm.tools.HttpClient;
import org.openstreetmap.josm.tools.OpenBrowser;

/**
 * Authenticate to OpenStreetMap (over damn server).
 *
 * @author qeef
 * @since xxx
 */
public class AuthenticateAction extends JosmAction {
    private String damn_url = Config.getPref().get("damn.server_url");
    private String damn_token = Config.getPref().get("damn.token");

    /**
     * Constructs a new {@code AuthenticateAction}.
     */
    public AuthenticateAction()
    {
        super(
            tr("Authenticate"),
            "actions/osm",
            tr("Authenticate to OpenStreetMap server."),
            null,
            true
        );
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        Random r = new Random();
        int tid_len = 64;
        StringBuilder tid_builder = new StringBuilder(tid_len);
        for (int i = 0; i < tid_len; i++) {
            int j = 97 + (int) (r.nextFloat() * (122 - 97 + 1));
            tid_builder.append((char) j);
        }
        String tid = tid_builder.toString();
        String auth_url = damn_url + "/auth/" + tid;
        OpenBrowser.displayUrl(auth_url);

        Timer t = new Timer();
        t.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run()
            {
                try {
                    HttpClient g = HttpClient.create(
                        new URL(damn_url + "/token/" + tid)
                    );
                    g.connect();
                    BufferedReader r = g.getResponse().getContentReader();
                    String c;
                    Pattern p = Pattern.compile("\"token\":\"(.+)\"");
                    while ((c = r.readLine()) != null) {
                        Matcher m = p.matcher(c);
                        if (m.find()) {
                            Config.getPref().put("damn.token", m.group(1));
                            t.cancel();
                            t.purge();
                        }
                    }
                } catch(Exception e) {
                }
            }
        }, 100, 100);
    }
}
