package org.openstreetmap.josm.plugins.damn.actions;

import static org.openstreetmap.josm.gui.help.HelpUtil.ht;
import static org.openstreetmap.josm.tools.I18n.tr;

import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.net.URL;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;

import org.openstreetmap.josm.actions.JosmAction;
import org.openstreetmap.josm.spi.preferences.Config;
import org.openstreetmap.josm.tools.HttpClient;

import org.openstreetmap.josm.plugins.damn.callbacks.PostReviewAreaCallback;

/**
 * Post review area query to damn server.
 *
 * @author qeef
 * @since xxx
 */
public class PostReviewAreaAction extends JosmAction {
    private String damn_url = Config.getPref().get("damn.server_url");
    private String damn_token = Config.getPref().get("damn.token");
    private PostReviewAreaCallback callback;
    JsonObject reviewArea;

    public PostReviewAreaAction(PostReviewAreaCallback c, JsonObject jo)
    {
        super(
            tr("Review"),
            "actions/review",
            tr("Post review area query to damn server."),
            null,
            true
        );
        callback = c;
        reviewArea = jo;
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        try {
            HttpClient g = HttpClient.create(
                new URL(
                    damn_url
                    + "/area/"
                    + reviewArea.getInt("aid")
                    + "/commits"
                ),
                "POST"
            );
            g.setHeader("Content-Type", "application/json");
            g.setHeader("Authorization", "Bearer " + damn_token);
            String ra = Config.getPref().get("damn.review_action");
            JsonObject map_area = Json.createObjectBuilder()
                .add("type", ra)
            .build();
            g.setRequestBody(map_area.toString().getBytes());
            g.connect();
            BufferedReader br = g.getResponse().getContentReader();
            JsonReader r = Json.createReader(br);
            JsonObject gotSquare = r.readObject();
            r.close();
            callback.onPostReviewArea(reviewArea, gotSquare);
        } catch (Exception ex) {
        }
    }
}
