package org.openstreetmap.josm.plugins.damn;

import static org.openstreetmap.josm.gui.help.HelpUtil.ht;
import static org.openstreetmap.josm.tools.I18n.tr;

import java.awt.Component;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collection;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;
import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.openstreetmap.josm.actions.downloadtasks.DownloadGeoJsonTask;
import org.openstreetmap.josm.actions.downloadtasks.DownloadNotesTask;
import org.openstreetmap.josm.actions.downloadtasks.DownloadOsmTask;
import org.openstreetmap.josm.actions.downloadtasks.DownloadParams;
import org.openstreetmap.josm.actions.UploadAction;
import org.openstreetmap.josm.data.coor.LatLon;
import org.openstreetmap.josm.data.gpx.GpxData;
import org.openstreetmap.josm.data.gpx.ImmutableGpxTrack;
import org.openstreetmap.josm.data.gpx.WayPoint;
import org.openstreetmap.josm.data.osm.DataSet;
import org.openstreetmap.josm.gui.layer.GpxLayer;
import org.openstreetmap.josm.gui.layer.Layer;
import org.openstreetmap.josm.gui.layer.OsmDataLayer;
import org.openstreetmap.josm.gui.MainApplication;
import org.openstreetmap.josm.gui.util.GuiHelper;
import org.openstreetmap.josm.gui.SideButton;
import org.openstreetmap.josm.gui.dialogs.ToggleDialog;
import org.openstreetmap.josm.plugins.Plugin;
import org.openstreetmap.josm.plugins.PluginInformation;
import org.openstreetmap.josm.spi.preferences.Config;
import org.openstreetmap.josm.spi.preferences.PreferenceChangeEvent;

import org.openstreetmap.josm.plugins.damn.actions.AuthenticateAction;
import org.openstreetmap.josm.plugins.damn.actions.GetAreaAction;
import org.openstreetmap.josm.plugins.damn.actions.GetAreaGeometryAction;
import org.openstreetmap.josm.plugins.damn.actions.GetAreasAction;
import org.openstreetmap.josm.plugins.damn.actions.GetWorkingOnAction;
import org.openstreetmap.josm.plugins.damn.actions.PostDoneSquareAction;
import org.openstreetmap.josm.plugins.damn.actions.PostMapAreaAction;
import org.openstreetmap.josm.plugins.damn.actions.PostMapSquareAction;
import org.openstreetmap.josm.plugins.damn.actions.PostReviewAreaAction;
import org.openstreetmap.josm.plugins.damn.actions.PostReviewSquareAction;
import org.openstreetmap.josm.plugins.damn.actions.PostSplitSquareAction;
import org.openstreetmap.josm.plugins.damn.actions.SetMapAction;
import org.openstreetmap.josm.plugins.damn.actions.SetReviewAction;
import org.openstreetmap.josm.plugins.damn.callbacks.GetAreaCallback;
import org.openstreetmap.josm.plugins.damn.callbacks.GetAreaGeometryCallback;
import org.openstreetmap.josm.plugins.damn.callbacks.GetAreasCallback;
import org.openstreetmap.josm.plugins.damn.callbacks.GetWorkingOnCallback;
import org.openstreetmap.josm.plugins.damn.callbacks.PostDoneSquareCallback;
import org.openstreetmap.josm.plugins.damn.callbacks.PostMapAreaCallback;
import org.openstreetmap.josm.plugins.damn.callbacks.PostMapSquareCallback;
import org.openstreetmap.josm.plugins.damn.callbacks.PostReviewAreaCallback;
import org.openstreetmap.josm.plugins.damn.callbacks.PostReviewSquareCallback;
import org.openstreetmap.josm.plugins.damn.callbacks.PostSplitSquareCallback;

/**
 * Base dialog for damn plugin.
 *
 * <p>
 * The client for Divide and map. Now. -- the damn project.
 * </p>
 *
 * @see "https://www.damn-project.org/"
 * @author qeef
 * @since xxx
 */
public class
    DamnDialog
extends
    ToggleDialog
implements
    PostDoneSquareCallback,
    PostSplitSquareCallback,
    PostReviewSquareCallback,
    PostMapSquareCallback,
    PostReviewAreaCallback,
    PostMapAreaCallback,
    GetAreaCallback,
    GetAreaGeometryCallback,
    GetWorkingOnCallback,
    GetAreasCallback
{
    private boolean _download_notes_automatically = false;
    private ScheduledExecutorService looper =
        Executors.newScheduledThreadPool(1);
    private ScheduledFuture<?> handle_looper;

    private String changeset_comment = "";

    /**
     * Constructs a new {@code DamnDialog}.
     */
    public DamnDialog()
    {
        super(
            tr("Divide and map. Now."),
            "damn",
            tr("Open the damn plugin dialog window."),
            null,
            150
        );
        JLabel intro = new JLabel("", 0);
        List<SideButton> buttons = new ArrayList<SideButton>();
        buttons.add(new SideButton(new SetMapAction("map recent")));
        buttons.add(new SideButton(new SetMapAction("map oldest")));
        buttons.add(new SideButton(new SetMapAction("map random")));
        buttons.add(new SideButton(new SetMapAction("map nearest")));
        buttons.add(new SideButton(new SetReviewAction("review recent")));
        buttons.add(new SideButton(new SetReviewAction("review oldest")));
        buttons.add(new SideButton(new SetReviewAction("review random")));
        buttons.add(new SideButton(new SetReviewAction("review nearest")));
        buttons.add(new SideButton(new SetReviewAction("review newbie")));
        createLayout(intro, true, buttons);
        authed();
    }

    /**
     * Check if token in preferences, authenticate otherwise.
     */
    public void authed()
    {
        JPanel buttons = (JPanel) getComponent(2);
        buttons.removeAll();
        try {
            JsonObject jwt = get_jwt(Config.getPref().get("damn.token"));
            String dn = jwt.getString("display_name");
            change1(new JLabel(tr("I am {0}.", dn), 0));
            change2(Arrays.asList(
                new SideButton(new GetAreasAction(this)),
                new SideButton(new GetWorkingOnAction(this)),
                new SideButton(new AuthenticateAction())
            ));
        } catch (Exception e) {
            String auth_t = tr("Please, authenticate to OpenStreetMap.");
            String after_t = tr(
                "After click to `Authenticate` button,"
                + " check out the web browser."
            );
            change1(
                new JLabel(
                    "<html>" + auth_t + "<br /><br />" + after_t + "</html>",
                    0
                )
            );
            change2(Arrays.asList(new SideButton(new AuthenticateAction())));
        }
        validate();
    }

    /**
     * Change component at index 1.
     *
     * This is the main component of the Toggle Dialog, i.e. label with
     * text or a list of something.
     */
    public void change1(Component data)
    {
        add(new JScrollPane(data), 1);
        remove(2);
    }

    /**
     * Change component at index 2.
     *
     * This is the buttons component -- change the buttons for the
     * buttons in `nb` collection.
     */
    public void change2(Collection<SideButton> nb)
    {
        JPanel buttons = (JPanel) getComponent(2);
        buttons.removeAll();
        for (SideButton b: nb)
            buttons.add(b);
    }

    /**
     * Decode JWT
     */
    public static JsonObject get_jwt(String token)
    {
        String[] blocks = token.split("\\.");
        String j = new String(Base64.getUrlDecoder().decode(blocks[1]));
        JsonReader r = Json.createReader(new StringReader(j));
        JsonObject jwt = r.readObject();
        r.close();
        return jwt;
    }

    public static List<WayPoint> coords_to_trkseg(String type, JsonArray coordinates)
    {
        List<WayPoint> trkseg = new ArrayList<>();
        try {
            switch (type) {
            case "MultiPolygon":
                for (JsonValue cv: coordinates) {
                    JsonArray c = (JsonArray) cv;
                    for (JsonValue jv: c.getJsonArray(0)) {
                        JsonArray ja = (JsonArray) jv;
                        trkseg.add(new WayPoint(new LatLon(
                            ja.getJsonNumber(1).doubleValue(),
                            ja.getJsonNumber(0).doubleValue()
                        )));
                    }
                }
                break;
            case "Polygon":
                trkseg = new ArrayList<>();
                for (JsonValue jv: coordinates.getJsonArray(0)) {
                    JsonArray ja = (JsonArray) jv;
                    trkseg.add(new WayPoint(new LatLon(
                        ja.getJsonNumber(1).doubleValue(),
                        ja.getJsonNumber(0).doubleValue()
                    )));
                }
                break;
            case "LineString":
                trkseg = new ArrayList<>();
                for (JsonValue jv: coordinates) {
                    JsonArray ja = (JsonArray) jv;
                    trkseg.add(new WayPoint(new LatLon(
                        ja.getJsonNumber(1).doubleValue(),
                        ja.getJsonNumber(0).doubleValue()
                    )));
                }
                break;
            }
        } catch (Exception e) {}
        return trkseg;
    }

    /**
     * Add new layer with square data.
     *
     * <p>
     * Add square border and download OpenStreetMap data.
     * </p>
     *
     * @param gotSquare A square from damn server.
     */
    public void addDamnLayer(JsonObject gotSquare) {
        // Add `gotSquare` border.
        GpxData border = new GpxData();
        Map<String, Object> trkAttr = new HashMap<>();
        Collection<Collection<WayPoint>> trk = new ArrayList<>();
        JsonArray coordinates;
        String border_type = "";
        String str_aid = "";
        String str_sid = "";
        try {
            coordinates = gotSquare
                .getJsonObject("border")
                .getJsonArray("coordinates")
            ;
            border_type = gotSquare
                .getJsonObject("border")
                .getString("type")
            ;
            str_aid = String.valueOf(gotSquare.getInt("aid"));
            str_sid = String.valueOf(gotSquare.getInt("sid"));
            trk.add(coords_to_trkseg(border_type, coordinates));
        } catch (Exception e) {
            try {
                for (JsonValue a: gotSquare
                        .getJsonArray("features")) {
                    JsonObject f = (JsonObject) a;
                    coordinates = f.getJsonObject("geometry").getJsonArray("coordinates");
                    border_type = f.getJsonObject("geometry").getString("type");
                    trk.add(coords_to_trkseg(border_type, coordinates));
                }
                str_aid = String.valueOf(gotSquare.getInt("aid"));
            } catch (Exception ee) {
                return;
            }
        }
        trkAttr.put(
            "name",
            "damn "
            + str_aid
            + ((str_sid != "") ? "/" + str_sid : "")
        );
        border.addTrack(new ImmutableGpxTrack(trk, trkAttr));
        GpxLayer gl = new GpxLayer(
            border,
            "damn "
            + str_aid
            + ((str_sid != "") ? "/" + str_sid : "")
        );
        gl.lock();
        gl.invalidate();
        MainApplication.getLayerManager().addLayer(gl);
        // Download OSM data.
        DownloadOsmTask dt = new DownloadOsmTask();
        Future<?> f = dt.download(
            new DownloadParams(),
            border.recalculateBounds(),
            null
        );
        MainApplication.worker.submit(() -> {
            try {
                f.get();
                if (!this.changeset_comment.equals("")) {
                    DataSet ds =
                        MainApplication.getLayerManager().getEditDataSet();
                    if (ds != null) {
                        ds.addChangeSetTag("comment", this.changeset_comment);
                    }
                }
            } catch (Exception e) {
            }
        });
        if (this._download_notes_automatically) {
            // Download OSM notes.
            try {
                this.looper.scheduleAtFixedRate(new Runnable() {
                    public void run() {
                        DownloadNotesTask ndt = new DownloadNotesTask();
                        Future<?> nf = ndt.download(
                            new DownloadParams(),
                            border.recalculateBounds(),
                            null
                        );
                        MainApplication.worker.submit(() -> {
                            try {
                                nf.get();
                            } catch (Exception e) {
                            }
                        });
                    }},
                    0,
                    Config.getPref().getLong(
                        "damn.download_notes_period_minutes", 10),
                    TimeUnit.MINUTES);
            } catch (Exception ee) {
                DownloadNotesTask ndt = new DownloadNotesTask();
                Future<?> nf = ndt.download(
                    new DownloadParams(),
                    border.recalculateBounds(),
                    null
                );
                MainApplication.worker.submit(() -> {
                    try {
                        nf.get();
                    } catch (Exception e) {
                    }
                });
            }
        }
    }

    /**
     * Remove layers added by {@code addDamnLayer}.
     */
    static public boolean removeDamnLayers() {
        try {
            OsmDataLayer dl = MainApplication
                .getLayerManager()
                .getActiveDataLayer()
            ;
            if (dl.requiresUploadToServer()) {
                (new UploadAction()).actionPerformed(null);
                return false;
            }
            dl.getDataSet().clear();
            dl.invalidate();
        } catch (Exception e) {
        }
        try {
            for (Layer la: MainApplication.getLayerManager().getLayers()) {
                if (la.getName().substring(0, 4).equals("damn"))
                    MainApplication.getLayerManager().removeLayer(la);
            }
        } catch (Exception e) {
        }
        return true;
    }

    // Callbacks
    /**
     * Show area info.
     */
    @Override
    public void onGetArea(JsonObject callArea, JsonObject gotArea)
    {
        // TODO remove, because remove config (it should be area dependent)
        this._download_notes_automatically = Config.getPref().getBoolean(
                "damn.download_notes_automatically");
        this.changeset_comment = gotArea.getString("tags");
        if (!this.changeset_comment.equals("")) {
            DataSet ds = MainApplication.getLayerManager().getEditDataSet();
            if (ds != null) {
                ds.addChangeSetTag("comment", this.changeset_comment);
            }
        }
        DamnDialog thisDamnDialog = this;
        GuiHelper.runInEDT(() -> {
        try {
            String area_t = tr(
                "Area {0} (priority {1})",
                String.valueOf(gotArea.getInt("aid")),
                String.valueOf(gotArea.getInt("priority"))
            );
            JsonObject desc = gotArea.getJsonObject("description");
            String desc_t = "";
            for (String k: desc.keySet()) {
                desc_t += "<h4>" + k + "</h4>";
                desc_t += "<p>" + desc.getString(k) + "</p>";
            }
            JsonObject instr = gotArea.getJsonObject("instructions");
            String instr_t = "<ul>";
            for (String k: instr.keySet()) {
                if (k.equals("_download_notes_automatically")) {
                    this._download_notes_automatically = Boolean.parseBoolean(
                            instr.getString(k));
                } else {
                    instr_t += "<li>";
                    instr_t += k;
                    instr_t += ": " + instr.getString(k);
                    instr_t += "</li>";
                }
            }
            instr_t += "</ul>";
            String ih = "";
            ih += "<html>";
            ih += "<h2>" + area_t + "</h2>";
            ih += "<p>" + gotArea.getString("tags") + "</p>";
            ih += "<hr />";
            ih += desc_t;
            ih += "<hr />";
            ih += instr_t;
            ih += "</html>";
            change1(new JLabel(ih, 0));
            change2(Arrays.asList(
                new SideButton(new PostMapAreaAction(
                    thisDamnDialog,
                    gotArea
                )),
                new SideButton(new PostReviewAreaAction(
                    thisDamnDialog,
                    gotArea
                )),
                new SideButton(new GetAreaGeometryAction(thisDamnDialog, gotArea)),
                new SideButton(new GetAreasAction(thisDamnDialog))
            ));
            validate();
        } catch (Exception e) {}
        });
    }

    /**
     * Show area geometry.
     */
    @Override
    public void onGetAreaGeometry(JsonObject gotAreaGeometry)
    {
        addDamnLayer(gotAreaGeometry);
    }

    /**
     * Show areas.
     */
    @Override
    public void onGetAreas(JsonArray ja)
    {
        DamnDialog thisDamnDialog = this;
        GuiHelper.runInEDT(() -> {
        try {
            DefaultListModel lm = new DefaultListModel();
            for (JsonValue a: ja) {
                JsonObject o = (JsonObject) a;
                String o_t = "";
                o_t += " ";
                int to_map = o.getInt("squares_to_map");
                int to_review = o.getInt("squares_to_review");
                int done = o.getInt("squares_done");
                int sum = to_map + to_review + done;
                o_t += Math.round(100 * to_map / sum);
                o_t += "% " + Math.round(100 * to_review / sum);
                o_t += "% " + Math.round(100 * done / sum);
                o_t += "% ";
                o_t += " ~ ";
                o_t += o.getInt("aid");
                o_t += " " + o.getString("tags");
                lm.addElement(o_t);
            }
            JList list = new JList(lm);
            list.setSelectionMode(0); // SINGLE_SELECTION
            list.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e)
                {
                    JList l = (JList) e.getSource();
                    if (
                        e.getButton() == MouseEvent.BUTTON1
                        && e.getClickCount() == 2
                    ) {
                        (new GetAreaAction(
                            thisDamnDialog,
                            (JsonObject) ja.get(l.getSelectedIndex())
                        )).actionPerformed(null);
                    }
                }
            });
            change1(list);
            change2(Arrays.asList(
                new SideButton(new GetAreasAction(thisDamnDialog)),
                new SideButton(new GetWorkingOnAction(thisDamnDialog)),
                new SideButton(new AuthenticateAction())
            ));
            validate();
        } catch (Exception e) {}
        });
    }

    /**
     * Show working on info.
     *
     * This represents user's commits since v0.2.0.
     */
    @Override
    public void onGetWorkingOn(JsonArray ja)
    {
        DamnDialog thisDamnDialog = this;
        GuiHelper.runInEDT(() -> {
        try {
            DefaultListModel lm = new DefaultListModel();
            for (JsonValue a: ja) {
                try {
                    JsonObject o = (JsonObject) a;
                    String o_t = "";
                    o_t += o.getInt("aid");
                    o_t += "/" + o.getInt("sid");
                    o_t += " ~ " + o.getString("message");
                    lm.addElement(o_t);
                } catch (Exception e) {
                }
            }
            JList list = new JList(lm);
            list.setSelectionMode(0); // SINGLE_SELECTION
            list.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e)
                {
                    JList l = (JList) e.getSource();
                    if (
                        e.getButton() == MouseEvent.BUTTON1
                        && e.getClickCount() == 2
                    ) {
                        (new GetAreaAction(
                            thisDamnDialog,
                            (JsonObject) ja.get(l.getSelectedIndex())
                        )).actionPerformed(null);
                    }
                }
            });
            change1(list);
            validate();
        } catch (Exception e) {}
        });
    }

    /**
     * Show area info after square marked done.
     */
    @Override
    public void onPostDoneSquare(JsonObject doneSquare, JsonObject gotSquare)
    {
        try {
            this.handle_looper.cancel(true);
        } catch (Exception e) {}
        DamnDialog thisDamnDialog = this;
        GuiHelper.runInEDT(() -> {
        try {
            (new GetAreaAction(
                thisDamnDialog,
                doneSquare
            )).actionPerformed(null);
        } catch (Exception e) {}
        });
    }

    /**
     * Show square info after map area request.
     */
    @Override
    public void onPostMapArea(JsonObject mapArea, JsonObject gotSquare)
    {
        DamnDialog thisDamnDialog = this;
        GuiHelper.runInEDT(() -> {
        try {
            String mapping_t = tr(
                "Mapping area {0} square {1}",
                String.valueOf(gotSquare.getInt("aid")),
                String.valueOf(gotSquare.getInt("sid"))
            );
            JsonObject instr = mapArea.getJsonObject("instructions");
            String instr_t = "<ul>";
            for (String k: instr.keySet()) {
                if (k.equals("_download_notes_automatically")) {
                    this._download_notes_automatically = Boolean.parseBoolean(
                            instr.getString(k));
                } else {
                    instr_t += "<li>";
                    instr_t += k;
                    instr_t += ": " + instr.getString(k);
                    instr_t += "</li>";
                }
            }
            instr_t += "</ul>";
            String ih = "";
            ih += "<html>";
            ih += "<h2>" + mapping_t + "</h2>";
            ih += instr_t;
            ih += "</html>";
            change1(new JLabel(ih , 0));
            change2(Arrays.asList(
                new SideButton(new PostMapSquareAction(
                    thisDamnDialog,
                    gotSquare
                )),
                new SideButton(new PostReviewSquareAction(
                    thisDamnDialog,
                    gotSquare
                )),
                new SideButton(new PostDoneSquareAction(
                    thisDamnDialog,
                    gotSquare
                )),
                new SideButton(new PostSplitSquareAction(
                    thisDamnDialog,
                    gotSquare
                ))
            ));
            validate();
        } catch (Exception e) {}
        });
        addDamnLayer(gotSquare);
        // Download tmp data from damn server.
        DownloadGeoJsonTask dt = new DownloadGeoJsonTask();
        Future<?> f = dt.loadUrl(
            new DownloadParams(),
            Config.getPref().get("damn.server_url")
            + "/area/"
            + String.valueOf(gotSquare.getInt("aid"))
            + "/square/"
            + String.valueOf(gotSquare.getInt("sid"))
            + "/tmp",
            null
        );
        MainApplication.worker.submit(() -> {
            try {
                f.get();
            } catch (Exception e) {
            }
        });
    }

    /**
     * Show area info after square marked to map.
     */
    @Override
    public void onPostMapSquare(JsonObject mapSquare, JsonObject gotCommit)
    {
        try {
            this.handle_looper.cancel(true);
        } catch (Exception e) {}
        DamnDialog thisDamnDialog = this;
        GuiHelper.runInEDT(() -> {
        try {
            (new GetAreaAction(
                thisDamnDialog,
                mapSquare
            )).actionPerformed(null);
        } catch (Exception e) {}
        });
    }

    /**
     * Show square info after review square request.
     */
    @Override
    public void onPostReviewArea(JsonObject reviewArea, JsonObject gotSquare)
    {
        DamnDialog thisDamnDialog = this;
        GuiHelper.runInEDT(() -> {
        try {
            String reviewing_t = tr(
                "Reviewing area {0} square {1}",
                String.valueOf(gotSquare.getInt("aid")),
                String.valueOf(gotSquare.getInt("sid"))
            );
            JsonObject instr = reviewArea.getJsonObject("instructions");
            String instr_t = "<ul>";
            for (String k: instr.keySet()) {
                if (k.equals("_download_notes_automatically")) {
                    this._download_notes_automatically = Boolean.parseBoolean(
                            instr.getString(k));
                } else {
                    instr_t += "<li>";
                    instr_t += k;
                    instr_t += ": " + instr.getString(k);
                    instr_t += "</li>";
                }
            }
            instr_t += "</ul>";
            String ih = "";
            ih += "<html>";
            ih += "<h2>" + reviewing_t + "</h2>";
            ih += instr_t;
            ih += "</html>";
            change1(new JLabel(ih , 0));
            change2(Arrays.asList(
                new SideButton(new PostMapSquareAction(
                    thisDamnDialog,
                    gotSquare
                )),
                new SideButton(new PostReviewSquareAction(
                    thisDamnDialog,
                    gotSquare
                )),
                new SideButton(new PostDoneSquareAction(
                    thisDamnDialog,
                    gotSquare
                )),
                new SideButton(new PostSplitSquareAction(
                    thisDamnDialog,
                    gotSquare
                ))
            ));
            validate();
        } catch (Exception e) {}
        });
        addDamnLayer(gotSquare);
    }

    /**
     * Show area info after marked to review request.
     */
    @Override
    public void onPostReviewSquare(
        JsonObject reviewSquare,
        JsonObject gotCommit
    )
    {
        try {
            this.handle_looper.cancel(true);
        } catch (Exception e) {}
        DamnDialog thisDamnDialog = this;
        GuiHelper.runInEDT(() -> {
        try {
            (new GetAreaAction(
                thisDamnDialog,
                reviewSquare
            )).actionPerformed(null);
        } catch (Exception e) {}
        });
    }

    /**
     * Show area after marked splitted request.
     */
    @Override
    public void onPostSplitSquare(
        JsonObject splitSquare,
        JsonObject splittedSquare
    )
    {
        try {
            this.handle_looper.cancel(true);
        } catch (Exception e) {}
        DamnDialog thisDamnDialog = this;
        GuiHelper.runInEDT(() -> {
        try {
            (new GetAreaAction(
                thisDamnDialog,
                splitSquare
            )).actionPerformed(null);
        } catch (Exception e) {}
        });
    }
}
