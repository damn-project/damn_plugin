package org.openstreetmap.josm.plugins.damn.callbacks;

import javax.json.JsonObject;

public interface GetAreaCallback {
    void onGetArea(JsonObject callArea, JsonObject gotArea);
}
