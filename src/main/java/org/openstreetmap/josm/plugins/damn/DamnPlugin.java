package org.openstreetmap.josm.plugins.damn;

import static org.openstreetmap.josm.gui.help.HelpUtil.ht;
import static org.openstreetmap.josm.tools.I18n.tr;

import javax.swing.JMenu;
import java.awt.event.KeyEvent;

import org.openstreetmap.josm.gui.MapFrame;
import org.openstreetmap.josm.plugins.Plugin;
import org.openstreetmap.josm.plugins.PluginInformation;
import org.openstreetmap.josm.spi.preferences.Config;

import org.openstreetmap.josm.plugins.damn.DamnDialog;

/**
 * Damn plugin -- client for Divide and Map. Now.
 *
 * @author qeef
 * @since xxx
 */
public class DamnPlugin extends Plugin
{
    /**
     * Constructs a new {@code DamnPlugin}.
     */
    public DamnPlugin(PluginInformation info)
    {
        super(info);
        try {
            String damn_url = Config.getPref().get("damn.server_url");
            if (damn_url.equals(""))
                throw new Exception();
        } catch(Exception e) {
            Config.getPref().put(
                "damn.server_url",
                "https://server.damn-project.org"
            );
        }
        try {
            String damn_token = Config.getPref().get("damn.token");
        } catch(Exception e) {
            Config.getPref().put("damn.token", "");
        }
        try {
            String damn_map_action = Config.getPref().get("damn.map_action");
            if (damn_map_action.equals(""))
                throw new Exception();
        } catch(Exception e) {
            Config.getPref().put("damn.map_action", "map random");
        }
        try {
            String damn_rev_act = Config.getPref().get("damn.review_action");
            if (damn_rev_act.equals(""))
                throw new Exception();
        } catch(Exception e) {
            Config.getPref().put("damn.review_action", "review recent");
        }
        try {
            boolean download_notes_auto = Config.getPref().getBoolean(
                "damn.download_notes_automatically");
        } catch(Exception e) {
            Config.getPref().putBoolean(
                "damn.download_notes_automatically",
                false);
        }
    }

    @Override
    public void mapFrameInitialized(MapFrame oldFrame, MapFrame newFrame)
    {
        if (newFrame != null && newFrame.mapView != null) {
            newFrame.addToggleDialog(new DamnDialog());
        }
    }
}
