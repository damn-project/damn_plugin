package org.openstreetmap.josm.plugins.damn.actions;

import static org.openstreetmap.josm.gui.help.HelpUtil.ht;
import static org.openstreetmap.josm.tools.I18n.tr;

import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.net.URL;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonValue;

import org.openstreetmap.josm.actions.JosmAction;
import org.openstreetmap.josm.spi.preferences.Config;
import org.openstreetmap.josm.tools.HttpClient;

import org.openstreetmap.josm.plugins.damn.callbacks.PostSplitSquareCallback;
import org.openstreetmap.josm.plugins.damn.DamnDialog;

/**
 * Split square query to damn server.
 *
 * @author qeef
 * @since xxx
 */
public class PostSplitSquareAction extends JosmAction {
    private String damn_url = Config.getPref().get("damn.server_url");
    private String damn_token = Config.getPref().get("damn.token");
    private PostSplitSquareCallback callback;
    JsonObject splitSquare;

    public PostSplitSquareAction(PostSplitSquareCallback c, JsonObject jo)
    {
        super(
            tr("Split"),
            "actions/split",
            tr("Split square query to damn server."),
            null,
            true
        );
        callback = c;
        splitSquare = jo;
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if (!DamnDialog.removeDamnLayers())
            return;
        try {
            HttpClient g = HttpClient.create(
                new URL(
                    damn_url
                    + "/area/" + splitSquare.getInt("aid")
                    + "/commits"
                ),
                "POST"
            );
            g.setHeader("Content-Type", "application/json");
            g.setHeader("Authorization", "Bearer " + damn_token);
            String msg;
            msg = tr("It's time to divide. Now.");
            JsonObject map_square = Json.createObjectBuilder()
                .add("sid", splitSquare.getInt("sid"))
                .add("type", "split")
                .add("message", msg)
            .build();
            g.setRequestBody(map_square.toString().getBytes());
            g.connect();
            BufferedReader br = g.getResponse().getContentReader();
            JsonReader r = Json.createReader(br);
            JsonObject splittedSquare = r.readObject();
            r.close();
            callback.onPostSplitSquare(splitSquare, splittedSquare);
        } catch (Exception ex) {
        }
    }
}
