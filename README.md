Damn plugin
===========

`damn_plugin` is part of **Divide and map. Now.** -- damn project. The purpose
of the project is to help mappers by dividing some big area into smaller
squares that a human can map.

`damn_plugin` is plugin for [JOSM][] that lets users contribute on
`damn_server` instance. The `damn_plugin` is based on [gradle-josm-plugin][].

License
-------

The project is published under [GNU GPLv3 license][].

[GNU GPLv3 license]: ./LICENSE
[JOSM]: https://josm.openstreetmap.de/
[gradle-josm-plugin]: https://gitlab.com/floscher/gradle-josm-plugin


Contribute
==========

Use [OneFlow][] branching model and keep the [changelog][].

Write [great git commit messages][]:

1. Separate subject from body with a blank line.
2. Limit the subject line to 50 characters.
3. Capitalize the subject line.
4. Do not end the subject line with a period.
5. Use the imperative mood in the subject line.
6. Wrap the body at 72 characters.
7. Use the body to explain what and why vs. how.

[OneFlow]: https://www.endoflineblog.com/oneflow-a-git-branching-model-and-workflow
[changelog]: ./CHANGELOG.md
[great git commit messages]: https://chris.beams.io/posts/git-commit/

Documentation
-------------

See generated [documentation](https://damn-project.gitlab.io/damn_plugin/).
